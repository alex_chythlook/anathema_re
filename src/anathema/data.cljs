(ns anathema.data
  (:require [reagent.core :as reagent]
            [anathema.character-elements :as cha-elm]
            [anathema.data-imp :as imp]))
(defn state-list-to-db [state-list]
  (imp/state-list-to-db-map state-list))


(defonce ^{:private true} data-state (reagent/atom (state-list-to-db imp/dev-state-list)))
(def ^{:private true} view (reagent/atom []))
(def ^{:private true} selections (reagent/atom {}))



(defn push-view [elemento]
  (swap! view (fn [a] (conj a elemento))))
(defn pop-view []
  (swap! view (fn [a] (pop a))))
(defn reset-view []
  (reset! view []))
(defn get-view []
  @view)

(defn get-element-in-view []
  ())