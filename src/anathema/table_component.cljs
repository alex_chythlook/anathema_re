(ns anathema.table-component
  (:require [clojure.string :as str]))

(def sample-data {:name "Hello" :type "World" :considerable "Worth"})
(def sample-rows [:name :type :considerable])
(defn table-row-for-data
  [column-keys-in-order individual-map]
  (into [:tr ]
        (map (fn [a]
               [:td (a individual-map)]
               ) column-keys-in-order)))
(defn column-headers [column-keys-in-order]
  (into [:tr ]
        (map (fn [a]
               [:th (-> a (name) (str/replace "-" " ") (str/capitalize))]
               ) column-keys-in-order)))
(defn table-for-data [column-keys-in-order seq-of-maps]
  [:table {:style {:overflow-y "scroll"
                   :display "block"}}
   [column-headers column-keys-in-order]
   [table-row-for-data column-keys-in-order seq-of-maps]])
