(ns anathema.styles
  (:require [garden.core :as g]
            [garden.color :as gc]
            [anathema.state :as state :refer [get-layout-state]]
            [clojure.string :as str]))

(def standard-border
  {:border-style :solid
   :border-color :black
   :border-width :2px})

(defn page-style []
  (g/css
    [:body {:background-color :white}]))

(defn url [stringer]
  (str "url(" stringer ")"))

(defn color-a [colori alphi]
  (let [{:keys [red green blue] :as color-map} (gc/as-rgb colori)]
    (str "rgba(" red ", " green ", " blue ", " alphi ")")))

(defn linear-gradient [& args]
  (str "linear-gradient"
       (-> (map #(-> %
                     name
                     (str ", ")
                     symbol) args)
           (str)
           (str/reverse)
           (str/replace-first " ," "")
           (str/reverse))
       ""))

(defn ? [thingy] (when thingy thingy))
(defn ?_ [thingy] (when thingy (str thingy " ")))
(defn shadow [& {:keys [h v b-radius color blur spread inset]}]
  (str h " " v " " (?_ b-radius) (?_ blur) (?_ spread) (?_ color) (?_ inset)))

(defn box [& {:keys [t r b l tb rl]}]
  (cond
    tb (str tb " " rl)
    rl (str t " " rl " " b)
    :default (str t " " r " " b " " l)))

(defn px [thingy]
  (str thingy "px"))
(defn % [thingy]
  (str thingy "%"))


(defn calc [a op b]
  (let [opmap {+ "+"
               - "-"
               * "*"
               / "/"}]
    (str "calc(" a " " (get opmap op) " " b ")")))

(def colors {:main-mid    "#718292"
             :main-light  "#A9C8DD"
             :main-dark   "#4A6473"
             :main-contra "#EBB97D"})

(def title-box-shadow
  (shadow :h 0 :v 0 :blur "10px" :spread "0px" :color "black"))

(def title-text-shadow
  (shadow :h "0px" :v "0px" :b-radius "10px" :color (:main-light colors)))

(def menu-bar-text-shadow
  (shadow :h "0px" :v "0px" :b-radius "7px" :color (:main-mide colors)))

(def menu-bar-inset-shadow
  (shadow :h 0 :v 0 :blur "10px" :spread "1px" :color "black" :inset "inset"))

(def title-box-z 30)

(def style-sheet-data
  [[:* {:color (:main-contra colors)}]

   [:body
    {:background-image      (url "images/sun_moon_background.jpg")
     :background-color      :black
     :background-position   :center
     :background-repeat     :no-repeat
     :background-attachment :fixed
     :background-size       :cover}]

   [:button
    {:border-radius    (px 10)
     :margin           (px 5)
     :border-style     :double
     :border-width     (px 4)
     :border-color     (:main-mid colors)
     :background-color (:main-light colors)
     :color            :black}]

   [:div#total-app-area
    {:width  :100%
     :height :auto}]

   [:div#app-title-area
    {;:border     :solid :1px (:main-1 colors)
     :height     :50px
     :position   :fixed
     :top        0
     :left       0
     :width      :100%
     :text-align :center
     :background (:main-mid colors)                         ;(linear-gradient (:main-mid colors) (:main-dark colors))
     :box-shadow title-box-shadow
     :z-index    title-box-z
     }
    [:*
     {:display :block
      :margin  "0 auto"
      }]
    [:h1
     {:margin      "5px auto"
      :color       :white
      :text-shadow title-text-shadow}]
    [:button
     {:position :relative
      :float    :left
      :left     (px 5)
      :top      (px 5)
      }]]


   [:div#app-search-area
    {:position   :fixed
     :top        "50px"
     :padding    "10px"
     :width      "100%"
     :background (:main-mid colors)                         ;(linear-gradient (:main-dark colors) (:main-mid colors))
     :box-shadow title-box-shadow
     :z-index    title-box-z
     }
    [:input
     {:width         "100%"
      :height        "3em"
      :border-radius "10px"}]]



   [:div#app-body-area
    {:border "solid black 2px"
     :padding (box :tb (px 3) :rl (px 6))
     :height "2000px"}
    [:&.no-menu-showing
     {:margin (box :t (px 60) :r (% 3) :l (% 3) :b (px 25))}]
    [:&.with-menu-showing
     {:margin (box :t (px 60) :r (% 3) :l (calc (px 300) + (% 3)) :b (px 25))}]]




   [:div#app-menu-bar
    {:position :fixed
     :top      (px 50)
     :z-index  (+ 10 title-box-z)
     :color    :white
     :width    (% 100)
     :height   (% 100)}
    [:*
     {:color      :white
      :width      (px 300)
     :margin (px 0)}]
    [:h2
     {:font-size "1.5em"}]
    [:ul
     {
      :background    (:main-dark colors)
      :border-radius (px 0)
      :box-shadow    menu-bar-inset-shadow
      :height (% 100)}
     [:li
      {:width      (px 300)
       :margin-left (px -40)
       :padding-left (px 40)
       :padding-right (px 30)
       :list-style :none
       :font-size   "3em"
       :text-align  :left
       :text-shadow menu-bar-text-shadow
       :border-bottom "double 1px black"
       }
      [:&:before
       {:background :black}]]]]

   ])

(defn compile-css []
  (->> style-sheet-data (map g/css) (map #(str % "\n")) (reduce str)))



(defn style-component []
  (println "Style Changes!")
  [:style {:type "text/css"} (compile-css)]
  )

(defn calc-dynamic-style-for [element-key]
  (element-key
    {:#app-body-area {:margin-top (px (if (state/get-layout-state :search) 120 60))}}))

(comment [:.side-bar (into
                       {:color    :black
                        :width    :200px
                        :position :fixed
                        :top      0
                        :left     0
                        :height   :100%}
                       standard-border)
          :ul {:margin :auto :padding 0}
          :li.file-button {:display  :inline
                           :position :relative
                           :margin   :auto
                           :padding  0}
          :.content-list {:overflow :scroll
                          :height   :40%}
          :.selection-button {:width :150px}
          :.app-area {:position     :fixed
                      :top          0
                      :bottom       0
                      :right        0
                      :left         :200px
                      :border       "2px solid black"
                      :display      :block
                      :flex-wrap    :wrap
                      :overflow-x   :scroll
                      :padding-left 0
                      }
          :.sheet-area {:width             :auto
                        :border            "2px solid black"
                        :display           :block
                        :margin            "0 0 0 0"
                        :column-width      :200px
                        :column-rule-style :none
                        }])
(comment [:div#app-menu-bar
          {:position      :fixed
           :bottom        (px -5)
           :left          0
           :margin        (px -5)
           :padding       (px 5)
           :width         (% 102)
           :height        (px 50)
           :background    (:main-dark colors)
           :border-radius (px 0)
           :box-shadow    menu-bar-inset-shadow}
          [:ul
           {:display :block
            :width   (% 100)
            :margin  "0 auto"
            :padding (px 0)
            :height  (px 100)}
           [:li
            {:display      :inline-block
             :width        (% 21)
             :height       (% 100)
             :margin-right (% 2)
             :margin-left  (% 2)
             ;:border "black 1px solid"
             ;:border-width (box :tb (px 0) :rl (px 2))
             ;:border-radius (px 5)
             ;:background :black
             :text-align   :center
             }
            [:button
             {:width      (% 100)
              :height     (px 30)
              :text-align "center top"
              :margin     "5px auto"
              :background :black
              :color      :white
              }
             ]]]])