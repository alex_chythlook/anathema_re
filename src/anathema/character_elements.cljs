(ns anathema.character-elements
  (:require [clojure.string :as str]))

(def attributes
  [:strength
   :dexterity
   :stamina
   :charisma
   :manipulation
   :appearance
   :perception
   :intelligence
   :wits])

(def abilities [:archery :athletics :awareness :brawl :bureaucracy :dodge :integrity :investigation :larceny
                :linguistics :lore :medicine :melee :occult :performance :presence :resistance :ride
                :sail :socialize :stealth :survival :thrown :war])

(def solar-castes [:twilight :dawn :night :zenith :eclipse])


(defn essence-level-given-used-xp [u]
  (cond
    (<= 0 u 49) 1
    (<= 50 u 124) 2
    (<= 125 u 199) 3
    (<= 200 u 299) 4
    :else 5))

(defn essence-pool-for-essence-level [u]
  {:personal   (+ 10 (* u 3))
   :peripheral (+ 26 (* u 7))})

(defn- boxes-for [thingy]
  (reduce (fn [v [a b]] (into v (take b (repeat a)))) [] thingy))
(defn health-track-for-health-module [health-module]
  (let [track-vec (sort (select-keys health-module [0 1 2 4]))
        damage-vec (select-keys health-module [:bashing :lethal :aggrivated])
        health-boxes (boxes-for track-vec)
        damage-boxes (boxes-for damage-vec)
        adjusted-damage-boxes (into damage-boxes (take (count health-boxes) (repeat nil)))
        combined-boxes (map (fn [a b] (if b b a)) health-boxes adjusted-damage-boxes)]
    combined-boxes))

(def volfer {:name               "Volfer"
             :type               :character
             :character-type     :solar
             :player             "timmy"
             :anima              "A great hulking, menacing figure"
             :concept            "Pit fighting hunk"
             :caste              :dawn
             :supernal           :melee
             :chron              "The Tomb of Dreams"
             :xp                 {:regular-total 100 :regular-used 30 :solar-total 40 :solar-used 10}
             :essence            {:personal 13 :peripheral 33 :committed 0}
             :attributes         {:strength     5
                                  :dexterity    3
                                  :stamina      4
                                  :charisma     2
                                  :manipulation 2
                                  :appearance   3
                                  :perception   3
                                  :intelligence 2
                                  :wits         3}
             :abilities          {:archery     0 :athletics 1 :awareness 3 :brawl 4
                                  :bureaucracy 0 :dodge 2 :integrity 3 :investigation 0 :larceny 2
                                  :linguistics 0 :lore 0 :medicine 1 :melee 5 :occult 0
                                  :performance 1 :presence 3 :resistance 4 :ride 0
                                  :sail        0 :socialize 0 :stealth 0 :survival 2 :thrown 0 :war 0}
             :specialties        [[:performance "Gladitorial Combat"] [:resistance "Poisoned Blades"]]
             :selected-abilities [:athletics :awareness :brawl :dodge :integrity
                                  :melee :performance :presence :resistance :survival]
             :willpower          {:total 6 :temporary 6}
             :intimacies         [["I enjoy bringing down the self-superior" 3] ["Companions (guarded trust)" 2] ["I resent challenges to my worth" 1]]
             :limit              {:trigger "Being mocked, humiliated, or belittled" :total 3}
             :merits             [["Mighty Thew" 3]
                                  ["Artifact" 3]
                                  ["Boundless Endurance" 2]
                                  ["Ambidexterous" 2]]
             :charms             ["Thunderclap Rush Attack"
                                  "Stubborn Boar Defense"
                                  "Excellent Strike"
                                  "Fire and Stones Strike"
                                  "One Weapon, Two Blows"
                                  "Tiger’s Dread Symmetry"
                                  "Durability of Oak Meditation"
                                  "Spirit Strengthens the Skin"
                                  "Ox-Body Technique"
                                  "Ox-Body Technique"]
             :evocations         [["Super-Hawt Swyping Attack" "Chopping Sword"]
                                  ["Fifth Finger Stops All Strikes" "Patchwork Armor"]]
             :weapons            [{:name "Unarmed" :acc 4 :dmg 7 :def 0 :ovw 1 :tags [:bashing :brawl :natural] :dice-pool 12}
                                  {:name "Chopping Sword" :acc 2 :dmg 9 :def 1 :ovw 1 :tags [:lethal :melee :chopping] :dice-pool 9}]
             :armor              [{:name "Patchwork Armor" :soak 3 :hardness 0 :mp 0 :tags []}]
             :health             {0 1, 1 4, 2 6, 4 1, :bashing 2, :lethal 1, :aggrivated 3}})

(def empty-charm
  {:name                ""
   :cost                ""
   :type                :reflexive
   :mins                [:brawl 3 1]
   :prerequisite-charms #{}
   :keywords            []
   :page                275
   :static-tags         []
   }
  )


(def empty-merit
  {:name           ""
   :possible-ranks #{1}
   :minimum        {:main {:ability :stamina :score 1} :alt {:ability :resistance :score 1}}
   :prereq-merits  #{}
   :page           158
   :static-tags    []
   }
  )

(def blank-artifact
  {:name ""
   :type :artifact-1
   }
  )

(def tomb-of-dreams
  {:name   "The Tomb of Dreams"
   :type   :chron
   :character-type :solar
   :charms [{:static-tags         [],
             :prerequisite-charms #{},
             :name                "Thunderclap Rush Attack",
             :type                :reflexive,
             :page                275,
             :min-rank            3,
             :keywords            [],
             :ability             :brawl,
             :cost                "3m",
             :min-essence         1}
            {:static-tags         [],
             :prerequisite-charms #{},
             :name                "Stubborn Boar Defense",
             :type                :permanent,
             :page                255,
             :min-rank            2,
             :keywords            [],
             :ability             :integrity,
             :cost                "-",
             :min-essence         1}
            {:static-tags         [],
             :prerequisite-charms #{},
             :name                "Excellent Strike",
             :type                :supplemental,
             :page                346,
             :min-rank            2,
             :keywords            [:uniform],
             :ability             :melee,
             :cost                "3m",
             :min-essence         1}
            {:static-tags         [],
             :prerequisite-charms #{"Excellent Strike"},
             :name                "Fire and Stones Strike",
             :type                :supplemental,
             :page                346,
             :min-rank            3,
             :keywords            [:dual],
             :ability             :melee,
             :cost                "1m per die or success",
             :min-essence         1}
            {:static-tags         [],
             :prerequisite-charms #{"Excellent Strike"},
             :name                "One Weapon, Two Blows",
             :type                :reflexive,
             :page                346,
             :min-rank            2,
             :keywords            [],
             :ability             :melee,
             :cost                "3m",
             :min-essence         1}
            {:static-tags         [],
             :prerequisite-charms #{"None"},
             :name                "Tiger’s Dread Symmetry",
             :type                :permanent,
             :page                370,
             :min-rank            3,
             :keywords            [],
             :ability             :presence,
             :cost                "—",
             :min-essence         1}
            {:static-tags         [],
             :prerequisite-charms #{},
             :name                "Durability of Oak Meditation",
             :type                :reflexive,
             :page                374,
             :min-rank            2,
             :keywords            [:dual],
             :ability             :resistance,
             :cost                "3m",
             :min-essence         1}
            {:static-tags         [],
             :prerequisite-charms #{"Durability of Oak Meditation"},
             :name                "Spirit Strengthens the Skin",
             :type                :reflexive,
             :page                374,
             :min-rank            2,
             :keywords            [:withering-only],
             :ability             :resistance,
             :cost                "1m per damage die removed",
             :min-essence         1}
            {:static-tags         [:ox-body],
             :prerequisite-charms #{"None"},
             :name                "Ox-Body Technique",
             :type                :permanent,
             :page                375,
             :min-rank            1,
             :keywords            [:stackable],
             :ability             :resistance,
             :repurchasable       true,
             :cost                "—",
             :min-essence         1}
            ]})

(comment ["Thunderclap Rush Attack"
          "Stubborn Boar Defense"
          "Excellent Strike"
          "Fire and Stones Strike"
          "One Weapon, Two Blows"
          "Tiger’s Dread Symmetry"
          "Durability of Oak Meditation"
          "Spirit Strengthens the Skin"
          "Ox-Body Technique"
          "Ox-Body Technique"])

