(ns anathema.components.app-face
  (:require [clojure.string :as str]
            [cljs.pprint :as pp :refer [pprint cl-format]]
            [cljs.tools.reader.edn :as edn]
            [garden.core :as garden]
            [anathema.character-elements :as ch-el]
            [anathema.util :as au]
            [anathema.state :as state :refer [app-state-new window-size get-selected-thing get-selected-section overwrite-selected-section! modal-showing-atoms set-showing-to! selection-atoms set-selected-to! clear-selected! chron-editor-showing-section chron-editor-editing-now chron-editor-tabs set-chron-editor-to make-chron-key]]
            [com.rpl.specter :as sp :refer [ALL ATOM FIRST LAST select transform keypath srange if-path pred]]
            [re-com.core :as re :refer [h-box v-box box gap line radio-button p h-split scroller selection-list input-text input-textarea single-dropdown checkbox radio-button button popover-anchor-wrapper popover-content-wrapper modal-panel horizontal-tabs]]
            [reagent.core :as reagent]
            [anathema.components.common :as ac :refer [weapon-editor armor-editor close-modal data-display close-modal side-bar-body minified-top-bar side-bar]]
            [anathema.components.character-sheet :as accom :refer [character-sheet]]
            [anathema.components.chron-editor :as acchron :refer [chron-editor-top chron-editor-bottom chron-charm-editor bulk-charm-addition]]
            [anathema.components.apps :as app :refer [app-bottom app-top]]
            [anathema.components.menu :as menu]
            [anathema.components.selection-view :as select]
            [anathema.styles :as style]
            [anathema.data :as data]))

(defn modals []
  (cond
    @(:data modal-showing-atoms)
    [modal-panel
     :backdrop-on-click (close-modal :data false)
     :style {}
     :child [data-display]]
    @(:edit-other modal-showing-atoms)
    [modal-panel
     :backdrop-on-click (close-modal :edit-other false)
     :child [side-bar-body]]
    (<= 0 @(:weapons modal-showing-atoms))
    [modal-panel
     :backdrop-on-click (close-modal :weapons -1)
     :child [weapon-editor]]
    (<= 0 @(:armor modal-showing-atoms))
    [modal-panel
     :backdrop-on-click (close-modal :armor -1)
     :child [armor-editor]]
    (<= 0 @((make-chron-key :charms) modal-showing-atoms))
    [modal-panel
     :backdrop-on-click (close-modal (make-chron-key :charms) -1)
     :child [chron-charm-editor]]
    @(:bulk-charms modal-showing-atoms)
    [modal-panel
     :backdrop-on-click (close-modal :bulk-charms false)
     :child [bulk-charm-addition]]
    :else [:p "Thanks for using! -Kenai"]
    ))

(defn app-area []
  (let [item-type (get-selected-section :type)
        head-content (case item-type
                       :chron [chron-editor-top]
                       :app [app-top]
                       [:div]
                       )
        scroll-content (case item-type
                         :chron [chron-editor-bottom]       ;[:p (prn-str (get-selected-section :charms))]
                         :character [character-sheet]
                         :app [app-bottom]
                         [:div "Please make a selection"])]
    [:div.super-app-area
     [:h1.selection-name (get-selected-section :name)]
     head-content
     scroll-content
     [modals]
     ]))

(defn calc-aspect-ratio [w h]
  (/ w h))

(defn app-face []

  (if true                                                  ;(> 1 (calc-aspect-ratio (:width @window-size) (:height @window-size)))
    [:div#total-app-area
     [style/style-component]
     [:div#app-title-area
      [:button#search-button {:onClick #(state/toggle-showing-modal :menu)} "Menu"]
      [:h1#app-title (get-selected-section :name)]]

     [:div#app-body-area
      {:class (if (state/get-layout-state :menu) "with-menu-showing" "no-menu-showing")}
      (case (:type (state/get-selected-thing))
        :category [select/selection-view (state/get-selected-thing)]
        [:p "Nothing selected yet"])]
     (if (state/get-layout-state :menu)
       [menu/menu-view (filter #(= :category (:type %)) (state/get-state-as-vec))])
     ]))

(comment [v-box
          :class "app-face"
          :children [[minified-top-bar]
                     [app-area]]]
         [h-split
          :height (str (- (:height @window-size) 0) "px")
          :panel-1 [side-bar]
          :panel-2 [app-area]
          :style {:margin 0}
          :class "app-face"
          :on-split-change (fn [n] (swap! window-size #(into % {:split-place n})))
          :initial-split 16])