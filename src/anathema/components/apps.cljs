(ns anathema.components.apps
  (:require [clojure.string :as str]
            [cljs.pprint :as pp :refer [pprint cl-format]]
            [cljs.tools.reader.edn :as edn]
            [garden.core :as garden]
            [anathema.character-elements :as ch-el]
            [anathema.util :as au]
            [anathema.state :as ast :refer [app-state-new window-size get-selected-thing get-selected-section overwrite-selected-section! modal-showing-atoms set-showing-to! selection-atoms set-selected-to! clear-selected! chron-editor-showing-section chron-editor-editing-now chron-editor-tabs set-chron-editor-to]]
            [com.rpl.specter :as sp :refer [ALL ATOM FIRST LAST select transform keypath srange if-path pred]]
            [re-com.core :as re :refer [h-box v-box box gap line radio-button p h-split scroller selection-list input-text input-textarea single-dropdown checkbox radio-button button popover-anchor-wrapper popover-content-wrapper modal-panel horizontal-tabs]]
            [reagent.core :as reagent]
            [anathema.components.common :as ac :refer [attribute-widgety critical-field selector slot-up slot-down]]
            [anathema.diceroller :as adr]))



(defn dice-roller-bottom [previous-roll-result]
  [input-textarea
   :model (adr/make-dice-roller-readout previous-roll-result)
   :on-change println
   :disabled? true
   :style {:height      400
           :font-family "monospace"}])

(defn roll-with-input-atoms [dn-atom reroll-atom double-atom]
  (let [roll-result (adr/roll-dice (int @dn-atom) @reroll-atom @double-atom)]
    (println roll-result)
    (overwrite-selected-section! :rolls (fn [a] (into a [roll-result])))))
(defn dice-roller-top []
  (let [reroll-atom (reagent/atom #{})
        double-atom (reagent/atom #{})
        dicenumber-atom (reagent/atom "3")
        make-checks
        (fn [atomo]
          (into [:div.roll-check-section]
                (map (fn [a] [checkbox
                              :label (str a)
                              :model (not (empty? (filter #(= a %) @atomo)))
                              :on-change (fn [b]
                                           (println atomo)
                                           (swap! atomo
                                                  (if b
                                                    (fn [c] (into c [a]))
                                                    (fn [c] (remove #(= a %) c))))
                                           atomo)])
                     (range 1 11))))]
    [:div.dice-roller-top
     [:div "Reroll"]
     [make-checks reroll-atom]
     [:div "Doubles"]
     [make-checks double-atom]
     [:div "Number of Dice"]
     [input-text
      :model dicenumber-atom
      :on-change #(reset! dicenumber-atom %)]
     [:button {:onClick (fn [a] (roll-with-input-atoms dicenumber-atom reroll-atom double-atom))}
      "Roll!"
      ]
     ]))

(def top-bar-height 180)
(defn app-top []
  [:div {:style {:height (str top-bar-height "px")}}
   [dice-roller-top]])

(defn app-bottom []
  [:div.app-area {:style {:height (str (- (:height @window-size) (+ top-bar-height 50)) "px")
                          :width  (ast/get-right-section)}}
   [dice-roller-bottom  (get-selected-section :rolls)]])