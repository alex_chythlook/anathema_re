(ns anathema.components.chron-editor
  (:require [clojure.string :as str]
            [cljs.pprint :as pp :refer [pprint cl-format]]
            [cljs.tools.reader.edn :as edn]
            [garden.core :as garden]
            [anathema.character-elements :as ch-el]
            [anathema.util :as au]
            [anathema.state :as ast :refer [app-state-new window-size get-selected-thing get-selected-section overwrite-selected-section! modal-showing-atoms set-showing-to! selection-atoms set-selected-to! clear-selected! chron-editor-showing-section chron-editor-editing-now chron-editor-tabs set-chron-editor-to make-chron-key]]
            [com.rpl.specter :as sp :refer [ALL ATOM FIRST LAST select transform keypath srange if-path pred]]
            [re-com.core :as re :refer [h-box v-box box gap line radio-button p h-split scroller selection-list input-text input-textarea single-dropdown checkbox radio-button button popover-anchor-wrapper popover-content-wrapper modal-panel horizontal-tabs md-icon-button md-circle-icon-button button]]
            [reagent.core :as reagent]
            [anathema.components.common :as ac :refer [table-for-data slot-up slot-down close-modal selector special-editor-number-section]]
            [anathema.text-data-conversion :as text]
            [cljs.core :as core]))

(def top-bar-height 140)

(def chron-query (reagent/atom "Ox-B"))
(defn chron-editor-top []
  (let [tab-id-fn hash
        tab-label-fn #(-> % name str/capitalize)
        compiled-tabs (map
                        (fn [a] {:label (tab-label-fn a) :id (tab-id-fn a)})
                        chron-editor-tabs)
        ]
    [:div.chron-editor {:style {:height (str top-bar-height "px")}}
     [horizontal-tabs
      :model (tab-id-fn @chron-editor-showing-section)
      :tabs compiled-tabs
      :on-change #(set-chron-editor-to %)]
     [input-text
      :class "chron-editor-query-field"
      :model chron-query
      :on-change println
      :change-on-blur? false]
     [:div.chron-editor-options-area "Lorum Ipsum"]
     [:div.chron-editor-control-panel
      [:button.incbutton {:onClick (slot-up :charms ch-el/empty-charm)} "+"]
      [:button.incbutton {:onClick (slot-down :charms ((make-chron-key :charms) selection-atoms))} "-"]
      [:button.incbutton {:onClick #(ast/set-showing-to! :bulk-charms true)} "Bulk Charm"]]
     ]))

(def charm-keys [:check :edit :name :ability :min-rank :min-essence :cost :type [:prerequisite-charms "Prereqs"] :keywords [:repurchasable "Repurchasable?"] :page :static-tags])

(def charm-types [:permanent :reflexive :ritual :supplemental :simple])


(defn charm-check-box [index]
  (let [charm-key (make-chron-key :charms)]
    [checkbox
     :model (@(charm-key selection-atoms) index)
     :on-change (fn [a]
                  (println "charm check " a)
                  (if a
                    (ast/add-to-selected charm-key index)
                    (ast/remove-from-selected charm-key index)))]))
(defn charm-edit-button [index]
  [button :label "✍️"
   :on-click
   (fn [a]
     (println "editing " index)
     (set-showing-to! (make-chron-key :charms) index))])

(def charm-change-fns
  (let [keyword-transformer #(-> % str (str/replace ":" "") (str/replace "-" " ") str/capitalize)]
    {:check               charm-check-box
     :edit                charm-edit-button
     :name                str
     :cost                str
     :type                keyword-transformer
     ;:mins                (fn [[a b c]] (str (keyword-transformer a) " " b " Essence " c))
     :ability             keyword-transformer
     :min-rank            str
     :min-essence         str
     :prerequisite-charms (fn [a] (reduce #(str % " ") a))
     :keywords            (fn [a] (reduce #(str (keyword-transformer %) " ") a))
     :page                str
     :static-tags         (fn [a] (reduce #(str (keyword-transformer %) " ") a))
     :repurchasable       (fn [a] (if a "Yes" "No"))
     }))

(defn reshape-charm-for-display [index charm]
  (-> charm (assoc :check index) (assoc :edit index))
  )
(defn get-charm-view [charms]
  (map #(reshape-charm-for-display % (nth charms %)) (range (count charms))))

(defn chron-editor-bottom []
  [:div.app-area {:style {:height (str (- (:height @window-size) (+ top-bar-height 50)) "px")}}
   [table-for-data
    charm-keys charm-change-fns (get-charm-view (get-selected-section :charms))
    ]
   [:div {:style {:height 50}}]])
(defn print-pass-second [a b]
                   (println a b)
                   b)
(defn chron-charm-editor []
  (let [{:keys [name cost type ability min-rank min-essence prerequisite-charms keywords page static-tags repurchasable] :as the-charm} (nth (get-selected-section :charms) @((make-chron-key :charms) modal-showing-atoms))
        charm-position-keypath (keypath @((make-chron-key :charms) modal-showing-atoms))]
    (println the-charm)
    ;(close-modal (make-chron-key :charms) -1)
    [:div.chron-charm-editor
     [:span.sheet-field
      [:label "Charm Name"]
      [input-text
       :class "charm-text select-text"
       :style {:flex 0}
       :model (print-pass-second "name is " name)
       :on-change #(overwrite-selected-section! :charms (keypath @((make-chron-key :charms) modal-showing-atoms)) :name name)]]
     [:span.sheet-field
      [:label "Ability"]
      [selector "chron-charm-selecoor" ch-el/abilities ability "chron-charm-editor-selector" #(overwrite-selected-section! :charms charm-position-keypath :ability %)]]
     [special-editor-number-section :charms (make-chron-key :charms) "Minimum Ability Rank" :min-rank min-rank]
     [special-editor-number-section :charms (make-chron-key :charms) "Minimum Essence" :min-essence min-essence]
     [:span.sheet-field
      [:label "Cost"]
      [input-text
       :model cost
       :on-change (fn [a] (overwrite-selected-section! :charms charm-position-keypath :cost a))]]
     [special-editor-number-section :charms (make-chron-key :charms) "Page" :page page]
     [:span.sheet-field
      [:label "Type"]
      [selector "chron-charm-selecoor" charm-types type "chron-charm-editor-selector" #(overwrite-selected-section! :charms charm-position-keypath :type %)]]
     [:span.sheet-field
      [:label "Keywords (';'-seperated)"]
      [input-text
       :model (text/seq-to-sss keywords #(-> % core/name str/capitalize))
       :on-change (fn [a] (overwrite-selected-section! :charms charm-position-keypath :keywords (text/sss-to-seq a [] #(-> % str/trim (str/replace " " "-") str/lower-case core/keyword))))]]
     [:span.sheet-field
      [:label "Prerequisite Charms (';'-seperated)"]
      [input-text
       :model (text/seq-to-sss prerequisite-charms str)
       :on-change (fn [a] (overwrite-selected-section! :charms charm-position-keypath :prerequisite-charms (text/sss-to-seq a [] str)))]]
     [:span.sheet-field
      [:label "Tags (';'-seperated) (Used internally by Anathema)"]
      [input-text
       :model (text/seq-to-sss static-tags #(-> % core/name str/capitalize))
       :on-change (fn [a] (overwrite-selected-section! :charms charm-position-keypath :static-tags (text/sss-to-seq a [] #(-> % str/trim (str/replace " " "-") str/lower-case core/keyword))))]]
     [:span.sheet-field
      [:label "Repurchasable?"]
      [checkbox
       :model repurchasable
       :on-change (fn [a] (overwrite-selected-section! :charms charm-position-keypath :repurchasable a))]]
     [:span.sheet-field
      [:label "Charm Text From Book"]
      [input-textarea
       :rows 60                                             ;
       :model ""
       :class "chron-charm-bulk-field"
       :on-change (fn [a]
                    (pprint a)
                    (println (text/charm-to-data (str a)))
                    (overwrite-selected-section! :charms charm-position-keypath (text/charm-to-data (str a))))]]
     ]
    ))

(defn bulk-charm-addition []
  (let [text-in-atom (reagent/atom "")]
    [:div.chron-bulk-charm-entry
     [:span "Input bulk charms. Seperate each charm block with one line of whitespace (hit 'enter' twice). When finished, click 'Commit Changes' to save. "]
     [input-textarea
      :rows 30
      :model ""
      :on-change #(reset! text-in-atom %)]
     [:span "Don't forget to check each one individually, as the import process is not perfect for all charms."]
     [:button {:style   {:width "100%"}
               :onClick (fn [_]
                          (overwrite-selected-section! :charms (fn [a] (into a (text/bulk-charm-to-data @text-in-atom)))))} "Commit Changes"]]))

(comment
  {:name                "Ox-Body Technique",
   :cost                "—",
   :mins                [:resistance 1 1],
   :type                :permanent,
   :keywords            [:stackable],
   :prerequisite-charms #{"None"},
   :static-tags         [:ox-body],
   :repurchasable       true
   :page                375})