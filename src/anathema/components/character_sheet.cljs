(ns anathema.components.character-sheet
  (:require [clojure.string :as str]
            [cljs.pprint :as pp :refer [pprint cl-format]]
            [cljs.tools.reader.edn :as edn]
            [garden.core :as garden]
            [anathema.character-elements :as ch-el]
            [anathema.util :as au]
            [anathema.state :as ast :refer [app-state-new window-size get-selected-thing get-selected-section overwrite-selected-section! modal-showing-atoms set-showing-to! selection-atoms set-selected-to! clear-selected! chron-editor-showing-section chron-editor-editing-now chron-editor-tabs set-chron-editor-to]]
            [com.rpl.specter :as sp :refer [ALL ATOM FIRST LAST select transform keypath srange if-path pred]]
            [re-com.core :as re :refer [h-box v-box box gap line radio-button p h-split scroller selection-list input-text input-textarea single-dropdown checkbox radio-button button popover-anchor-wrapper popover-content-wrapper modal-panel horizontal-tabs]]
            [reagent.core :as reagent]
            [anathema.components.common :as ac :refer [attribute-widgety critical-field selector slot-up slot-down]]))

(defn attribute-area
  ([attribute-key character-attributes] (attribute-area attribute-key character-attributes (range 6)))
  ([attribute-key character-attributes value-range]
   (let [change-fn (fn [a b]
                     (overwrite-selected-section! attribute-key (assoc character-attributes a b)))]
     (into [:ul] (map (fn [a] [attribute-widgety a value-range change-fn]) character-attributes)))))

(defn char-sheet-critical-area []
  (let []
    [:li {:class "sheet-area content-area"}
     [critical-field :name "Name"]
     [critical-field :player "Player"]

     ;
     ;[:div [:label {:for "player"} "Player"] [:input {:class "sheet-field", :type "text", :name "player", :defaultValue (player-bit :player)}]]
     [critical-field :concept "Concept"]
     [:div.sheet-field.critical-widget
      [:label.critical-label {:for "supernal" :style {:width "60px"}} "Supernal"]
      (selector "supernal" ch-el/abilities (get-selected-section :supernal) "critical-field" #(overwrite-selected-section! :supernal (keyword %)))]
     [:div.sheet-field.critical-widget
      [:label.critical-label {:for "caste" :style {:width "60px"}} "Caste"]
      (selector "caste" ch-el/solar-castes (get-selected-section :caste) "critical-field" #(overwrite-selected-section! :caste (keyword %)))
      ]
     [critical-field :anima "Anima "]]))

(defn limit-area [limit-module]
  (println limit-module)
  [:li#Limit-Area.sheet-area.content-area
   [:ul
    [:li.sheet-field [input-text
                      :class "limit-text select-text"
                      :style {:flex 0}
                      :model (:trigger limit-module)
                      :on-change #(overwrite-selected-section! :limit :trigger %)]]
    (attribute-widgety [:total (:total limit-module)] (range 0 10) (fn [a b]
                                                                     (println "limit is " a b)
                                                                     (overwrite-selected-section! :limit a b)))
    ]])

(defn experience-widget [[keyman valy]]
  (let [fieldname (str (name keyman) "-field")]
    [:div
     [:label.xp-label {:for fieldname} (str/capitalize (str/replace (name keyman) "-" " "))]
     [input-text
      :class "xp-text select-text"
      :style {:flex 0}
      :model (str valy)
      :on-change #(overwrite-selected-section! :xp keyman (int %))]]))
(defn experience-area [experience-module]
  (into [:li#Exprience-Area.sheet-area.content-area]
        (map #(experience-widget %)
             experience-module)))

(defn essence-widget [keyman current max]
  (println "current for " keyman " is " current " out of " max)
  [:div [:span.essence-label (str/capitalize (name keyman))] [input-text
                                                              :class "essence-text select-text"
                                                              :style {:flex 0}
                                                              :model (str current)
                                                              :on-change #(overwrite-selected-section! :essence keyman (int %))] (if max [:span (str " | ") max] [:span])])
(defn essence-area [essence-module calculated-essence-level]
  (let [{:keys [personal peripheral]} (ch-el/essence-pool-for-essence-level calculated-essence-level)]
    (println essence-module)
    [:li#Essence-Area.sheet-area.content-area
     [:div {:style {:height "2em"}} "Level " calculated-essence-level]
     [essence-widget :committed (:committed essence-module) nil]
     [essence-widget :personal (:personal essence-module) personal]
     [essence-widget :peripheral (:peripheral essence-module) (- peripheral (:committed essence-module))]
     ]))

(defn selected-ability-list [selected-list]
  (let [range-o (range 10)]
    (into [:ul] (map (fn [n] (selector (str "selected-" n) ch-el/abilities (nth selected-list n) ""
                                       (fn [i] (overwrite-selected-section! :selected-abilities (keypath n) (keyword i))))) range-o))
    ))

(defn multi-select-module [section-size class-string set-atomo make-new-fn]
  (into [:div] (map (fn [n] [:div {:class (str "sheet-field select-field " class-string)}
                             ^{:key (keyword (str class-string "-field-" n))}
                             [checkbox
                              :model (@set-atomo n)
                              :on-change (fn [b] (swap! set-atomo
                                                        (fn [x]
                                                          (if b (into #{n} x) (into #{} (remove #(= % n) x))))))
                              :label (make-new-fn n)
                              ]])
                    (range section-size))))
(defn merit-module [merit-seq]
  (let [make-new-section-fn (fn [n]
                              (let [[merit-text merit-value] (nth merit-seq n)]
                                [:span [input-text
                                        :class "merit-text select-text"
                                        :style {:flex 0}
                                        :model merit-text
                                        :on-change #(overwrite-selected-section! :merits (keypath n) FIRST %)]
                                 [selector (str "selector-" n) (map str (range 6)) merit-value "merit-selector" #(overwrite-selected-section! :merits (keypath n) LAST %)]]))]

    [multi-select-module (count merit-seq) "merit-field" (:merits selection-atoms) make-new-section-fn]
    ))

(defn charm-module []
  (let [make-new-section-fn (fn [n]
                              (let [charm-text (nth (get-selected-section :charms) n)]
                                [:span [input-text
                                        :class "charm-text select-text"
                                        :style {:flex 0}
                                        :model charm-text
                                        :on-change #(overwrite-selected-section! :charms (keypath n) %)]]))]

    [multi-select-module (count (get-selected-section :charms)) "charm-field" (:charms selection-atoms) make-new-section-fn]
    ))

(defn evocation-module []
  (let [make-new-section-fn (fn [n]
                              (let [[evocation-text artifact-name] (nth (get-selected-section :evocations) n)]
                                [:div [input-text
                                       :class "evocation-text select-text"
                                       :style {:flex 0}
                                       :model evocation-text
                                       :on-change #(overwrite-selected-section! :evocations (keypath n) FIRST %)]
                                 [selector "evocations-artifacts-selector"
                                  (map :name (into (get-selected-section :weapons) (get-selected-section :armor)))
                                  artifact-name "evocation-artifacts-selector" #(overwrite-selected-section! :evocations (keypath n) FIRST %)]]
                                ))]

    [multi-select-module (count (get-selected-section :evocations)) "charm-field" (:evocations selection-atoms) make-new-section-fn]
    ))

(def specialty-selections (reagent/atom #{}))
(defn specialty-module [specialty-seq]
  (let [make-new-section-fn (fn [n]
                              (let [[spec-ability spec-text] (nth specialty-seq n)]

                                [:span
                                 [selector (str "selector-" n) ch-el/abilities spec-ability "specialty-selector" #(overwrite-selected-section! :specialties (keypath n) FIRST %)]
                                 [input-text
                                  :class "specialty-text select-text"
                                  :style {:flex 0}
                                  :model spec-text
                                  :on-change #(overwrite-selected-section! :specialties (keypath n) LAST %)]
                                 ]))]

    [multi-select-module (count specialty-seq) "specialty-field" specialty-selections make-new-section-fn]
    ))

(defn intimacy-module [intimacy-seq]
  (let [make-new-section-fn (fn [n]
                              (let [[intimacy-text intimacy-value] (nth intimacy-seq n)]
                                [:div [input-text
                                       :class "intimacy-text select-text"
                                       :style {:flex 0}
                                       :model (str intimacy-text)
                                       :on-change #(overwrite-selected-section! :intimacies (keypath n) FIRST %)]
                                 [selector (str "selector-" n) (map str (range 1 4)) intimacy-value "intimacy-selector" #(overwrite-selected-section! :intimacies (keypath n) LAST %)]]))]

    [multi-select-module (count intimacy-seq) "intimacy-field" (:intimacies selection-atoms) make-new-section-fn]
    ))


(defn weapons-module [weapons-seq]
  (let [make-new-section-fn (fn [n]
                              (let [{:keys [name acc dmg def ovw dice-pool] :as weapon} (nth weapons-seq n)
                                    cma ", "]

                                [:span
                                 [button
                                  :style {:flex 0}
                                  :label (str name cma acc cma dmg cma def cma ovw cma dice-pool)
                                  :on-click #(set-showing-to! :weapons n)
                                  ;:tooltip "name, acc, dmg, def, ovw, dice pool"
                                  ]
                                 ]))]

    [multi-select-module (count weapons-seq) "weapons-field" (:weapons selection-atoms) make-new-section-fn]
    ))


(defn armor-module [armor-seq]
  (let [make-new-section-fn (fn [n]
                              (let [{:keys [name soak hardness mp] :as armor} (nth armor-seq n)
                                    cma ", "]

                                [:span
                                 [button
                                  :style {:flex 0}
                                  :label (str name cma soak cma hardness cma mp)
                                  :on-click (fn [] (println "editing armor " n) (set-showing-to! :armor n))
                                  ;:tooltip "name, soak, hardness, mp"
                                  ]
                                 ]))]

    [multi-select-module (count armor-seq) "armor-field" (:armor selection-atoms) make-new-section-fn]
    ))

(def health-level-to-add (reagent/atom 0))
(def damage-level-to-add (reagent/atom :bashing))
(defn health-area [health-module]
  (let [{zero 0, one 1, two 2, four 4} health-module
        health-track (ch-el/health-track-for-health-module health-module)
        mod-this (fn [fnny atomo] (assoc health-module @atomo (fnny (get health-module @atomo))))
        click-mod (fn [fnny atomo] (fn [] (overwrite-selected-section! :health (mod-this fnny atomo))))]
    (println health-level-to-add)
    (println zero one two four)
    [:li#Health-Area.sheet-area.content-area
     [:div#Health-Control-Panel
      [:span "Health Levels " (selector "Health Level To Add" ["0" "1" "2" "4"] @health-level-to-add "health-level-selector" #(reset! health-level-to-add (int %)))
       [:button.incbutton {:onClick (click-mod inc health-level-to-add)} "+"]
       [:button.decbutton {:onClick (click-mod dec health-level-to-add)} "-"]]
      [:span "  Damage " (selector "Damage To Add" (map name [:bashing :lethal :aggrivated]) @damage-level-to-add "damage-to-add-selector" #(reset! damage-level-to-add (keyword %)))
       [:button.incbutton {:onClick (click-mod inc damage-level-to-add)} "+"]
       [:button.decbutton {:onClick (click-mod dec damage-level-to-add)} "-"]]]

     (into [:div#Health-Track] (map (fn [a] [:span.health-box {:class (str "health-box-" (str/replace (str a) ":" ""))} [:span a]]) health-track))]))

(defn character-sheet []
  (let [calc-essence-level (ch-el/essence-level-given-used-xp (:regular-used (get-selected-section :xp)))]
    [:ul.app-area {:class "char-sheet solar-sheet"
                   :style {:height (str (- (:height @window-size) 50) "px")}}
     [:li.sheet-area.area-title "Character"]
     [char-sheet-critical-area]
     [:li.sheet-area.area-title "Attributes"]
     [:li {:class "sheet-area content-area", :id "Attribute-Area"}
      [attribute-area :attributes (get-selected-section :attributes) (range 1 6)]
      ]
     [:li.sheet-area.area-title "Abilities"]
     [:li {:class "sheet-area content-area", :id "Ability-Area"}
      ;(attribute-area :abilities ch-el/abilities attribute-change-fn)
      [attribute-area :abilities (get-selected-section :abilities)]
      ]
     [:li.sheet-area.area-title "Caste and Favored Abilities"]
     [:li#Selected-Area.sheet-area.content-area
      [selected-ability-list (get-selected-section :selected-abilities)]]
     [:li.sheet-area.area-title "Specialties"
      [:button.incbutton {:onClick (slot-up :specialties [:integrity "Breathing"])} "+"] [:button.incbutton {:onClick (slot-down :specialties specialty-selections)} "-"]]
     [:li#Selected-Area.sheet-area.content-area
      [specialty-module (get-selected-section :specialties)]]
     [:li.sheet-area.area-title "Essence"]
     [essence-area (get-selected-section :essence) calc-essence-level]
     [:li.sheet-area.area-title "Willpower"]
     [:li#Selected-Area.sheet-area.content-area             ;[willpower-section]
      [attribute-area :willpower (get-selected-section :willpower) (range 1 11)]]
     [:li.shet-area.area-title "Intimacies" [:button.incbutton {:onClick (slot-up :intimacies ["I like pie" 1])} "+"] [:button.incbutton {:onClick (slot-down :intimacies (:intimacies selection-atoms))} "-"]]
     [:li#Intimacies-Area.sheet-area.content-area [intimacy-module (get-selected-section :intimacies)]]
     [:li.sheet-area.area-title "Limit"]
     [limit-area (get-selected-section :limit)]
     [:li.sheet-area.area-title "Merits" [:button.incbutton {:onClick (slot-up :merits ["New Merit" 3])} "+"] [:button.incbutton {:onClick (slot-down :merits (:merits selection-atoms))} "-"]]
     [:li#Merit-Area.sheet-area.content-area
      [merit-module (get-selected-section :merits)]]
     [:li.sheet-area.area-title "Charms" [:button.incbutton {:onClick (slot-up :charms "New Charm")} "+"] [:button.incbutton {:onClick (slot-down :charms (:charms selection-atoms))} "-"]]
     [:li#Charm-Area.sheet-area.content-area
      [charm-module]]
     [:li.sheet-area.area-title "Weapons" [:button.incbutton {:onClick (slot-up :weapons {:name "New Weapon" :acc 4 :dmg 7 :def 0 :ovw 1 :tags [:bashing :brawl :natural] :dice-pool 12})} "+"] [:button.incbutton {:onClick (slot-down :weapons (:weapons selection-atoms))} "-"]]
     [:li#Weapon-area.sheet-area.content-area
      [weapons-module (get-selected-section :weapons)]]
     [:li.sheet-area.area-title "Armor" [:button.incbutton {:onClick (slot-up :armor {:name "New Armor" :soak 3, :hardness 0, :mp 0, :tags []})} "+"] [:button.incbutton {:onClick (slot-down :armor (:armor selection-atoms))} "-"]]
     [:li#Armor-area.sheet-area.content-area
      [armor-module (get-selected-section :armor)]]
     [:li.sheet-area.area-title "Evocations" [:button.incbutton {:onClick (slot-up :evocations ["New Evocation" "Unarmed"])} "+"] [:button.incbutton {:onClick (slot-down :evocations (:evocations selection-atoms))} "-"]]
     [:li#Evocation-Area.sheet-area.content-area
      [evocation-module]]
     [:li.sheet-area.area-title "Health"]
     [health-area (get-selected-section :health)]
     [:li.sheet-area.area-title "Experience"]
     [experience-area (get-selected-section :xp)]
     [:li.sheet-area.area-title "Derived values (will be implimented after chrons, as it relies heavily on chron information)"]
     [:li.sheet-area.content-area
      "Natural Soak: Stamina + Charms • Parry: ([Dexterity + Brawl, Martial Arts or Melee, whichever is appropriate to the character’s current armament] / 2, round up) + Weapon’s Defenses bonus\nEvasion: ([Dexterity + Dodge] / 2, round up) – armor’s mobility penalty • Resolve: ([Wits + Integrity + specialty] / 2, round up) • Guile: ([Manipulation + Socialize + specialty] / 2, round up)\n"]

     [:li.empty-thing {:style {:height 50}}]]))