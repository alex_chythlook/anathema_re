(ns anathema.components.common
  (:require [clojure.string :as str]
            [cljs.pprint :as pp :refer [pprint cl-format]]
            [cljs.tools.reader.edn :as edn]
            [garden.core :as garden]
            [anathema.character-elements :as ch-el]
            [anathema.util :as au]
            [anathema.state :as state :refer [app-state-new window-size get-selected-thing get-selected-section overwrite-selected-section! modal-showing-atoms set-showing-to! selection-atoms set-selected-to! clear-selected! chron-editor-showing-section chron-editor-editing-now chron-editor-tabs set-chron-editor-to]]
            [com.rpl.specter :as sp :refer [ALL ATOM FIRST LAST select transform keypath srange if-path pred]]
            [re-com.core :as re :refer [h-box v-box box gap line radio-button p h-split scroller selection-list input-text input-textarea single-dropdown checkbox radio-button button popover-anchor-wrapper popover-content-wrapper modal-panel horizontal-tabs]]
            [reagent.core :as reagent]))


(defn get-side-bar-width ([] (get-side-bar-width (fn [n] n)))
  ([modfn]
   (str (modfn (- (* (:width @window-size) (* .01 (:split-place @window-size))) 10)) "px")))

(defn selection-list-builder [title-string filter-fn]
  (let [character-name-list (->> @app-state-new (map last) (filter filter-fn) (map :name))
        new-keyword (keyword (str "ul#" title-string "-list.content-list"))
        character-ul (into [:ul]
                           (map #(into [:li] [[:button.selection-button {:style {:width (get-side-bar-width)} :on-click (fn [a] (state/switch-editing-focus-to %))} %]]) character-name-list))]

    [scroller
     :v-scroll :auto
     :height "40%"
     :width (get-side-bar-width)
     :child character-ul]))



(defn side-bar-body []
  [v-box
   :children [[:h3 "Characters"]
              (selection-list-builder "Characters" #(= (:type %) :character))
              [:h3 "Chronicles"]
              (selection-list-builder "Chronicles" #(= (:type %) :chron))
              [:h3 "Apps"]
              (selection-list-builder "Apps" #(= (:type %) :app))]])
(defn side-bar []
  (let [button-props {:style {:width (get-side-bar-width #(/ % 4))}}
        ]
    [v-box
     :height "500px"
     :width (get-side-bar-width)
     :children [[:ul.file-buttons
                 [:li.file-button
                  [:button#loadBtn button-props "Load"]]
                 [:li.file-button
                  [:button#saveBtn button-props "Save"]]
                 [:li {:class "file-button"}
                  [:button#newBtn button-props "New"]]
                 [:li.file-button
                  [:button#peekBtn (into button-props {:on-click #(reset! (:data modal-showing-atoms) true)}) "Data"]

                  ]]

                [side-bar-body]]]))


(defn minified-top-bar []
  (let [button-props {:style {:width (#(/ % 5.5) (:width @window-size))}}
        ]
    [:ul.file-buttons
     [:li.file-button
      [:button#loadBtn button-props "Load"]]
     [:li.file-button
      [:button#saveBtn button-props "Save"]]
     [:li {:class "file-button"}
      [:button#newBtn button-props "New"]]
     [:li.file-button
      [:button#peekBtn (into button-props {:on-click #(reset! (:data modal-showing-atoms) true)}) "Data"]]
     [:li.file-button
      [:button#peekBtn (into button-props {:on-click #(reset! (:edit-other modal-showing-atoms) true)}) "Edit Other"]]]))

(defn attribute-widgety [[attribute-spcific-key attribute-value] value-range changefn]
  (let [make-attribute-id (fn [a] (-> attribute-spcific-key (name) (str "-" a "-radio")))]
    [:li.sheet-field [:label (str/capitalize (name attribute-spcific-key))]
     ;(println "attribute is " attribute-spcific-key " value is " attribute-value)
     (into [:span] (map (fn [a]
                          (let [numeric-class-name (str "selected-" a)
                                radio-att-map {:type "radio" :name (str (name attribute-spcific-key) "-buttons") :value a :on-change #(changefn attribute-spcific-key a)}]
                            ^{:key (make-attribute-id a)}
                            [:span {:class (str (if (<= a attribute-value) "selected-radio-restyle " "") "radio-restyle " numeric-class-name)}
                             [:input.attribute-radio (if (= a attribute-value)
                                                       (into radio-att-map {:default-checked :checked})
                                                       radio-att-map)
                              ]
                             [:label {:for (make-attribute-id a)
                                      }
                              [:button {:on-click #(changefn attribute-spcific-key a)} " "]]])
                          )
                        value-range))]))
(comment
  [:span {:class    (if (<= a attribute-value) "selected-radio-restyle radio-restyle" "radio-restyle")
          :on-click #(changefn attribute-spcific-key %)}
   [:input]]
  [radio-button
   :model attribute-value
   :value a
   :label ""
   :label-class (if (<= a attribute-value) "selected-radio-restyle radio-restyle" "radio-restyle")
   :on-change #(changefn attribute-spcific-key %)])


(defn number-spinner [{:keys [model min max class on-change]}]
  (println "model " model)
  [:input {:type "number" :min min :max max :class class :on-change #(on-change (-> % .-target .-value))} model])

(defn selector
  ([nameo list-o current-val] (selector nameo list-o current-val "sheet-selector"))
  ([nameo list-o current-val class-string] (selector nameo list-o current-val "sheet-selector" (fn [e] e)))
  ([nameo list-o current-val class-string change-fn]
   (into [:select.sheet-field {:name      nameo :defaultValue current-val :class class-string
                               :on-change #(change-fn (-> % .-target .-value))}]
         (map (fn [n] [:option {:value n} (str/capitalize (name n))]) list-o))))

(defn critical-field [section-key name-string]
  [:div.sheet-field.critical-widget
   [:label.critical-label {:style {:width "60px"}} name-string]
   [input-text
    :class "select-text critical-field"
    :model (get-selected-section section-key)
    :on-change #(overwrite-selected-section! section-key %)]])

(defn text-box [starting-text class-string on-change]
  )









(defn str-to-tags [t]
  (into [] (->> (str/split t ",")
                (map str/trim)
                (map #(str/replace % " " "-"))
                (map str/lower-case)
                (map keyword))))
(defn tags-to-str [t]
  (-> (->> t
           (map #(-> % str
                     (str/replace ":" "")
                     (str/capitalize)
                     (str " ")))
           (reduce str))
      (str/trim)
      (str/replace
        " " ", ")
      (str/replace "-" " ")))

(defn special-editor-number-section
  ([cat-key nameo keyw valli]
   (special-editor-number-section cat-key cat-key nameo keyw valli))
  ([cat-key modal-key nameo keyw valli]
   [:span.sheet-field
    [:label.critical-label nameo]
    [input-text
     :model (str valli)
     :class "special-number select-text critical-field"
     :on-change (fn [a] (overwrite-selected-section! cat-key (keypath @(modal-key modal-showing-atoms)) keyw (int a)))]]))
;{:name "Unarmed" :acc 4 :dmg 7 :def 0 :ovw 1 :tags [:bashing :brawl :natural] :dice-pool 12}
(defn weapon-editor []
  (let [{:keys [name acc dmg def ovw tags dice-pool] :as the-weapon} (nth (get-selected-section :weapons) @(:weapons modal-showing-atoms))]
    (println the-weapon)
    [:div
     [:span.sheet-field
      [:label "Weapon Name"]
      [input-text
       :class "weapons-text select-text"
       :style {:flex 0}
       :model name
       :on-change #(overwrite-selected-section! :weapons (keypath @(:weapons modal-showing-atoms)) :name name)]
      ]
     [special-editor-number-section :weapons "Accuracy" :acc acc]
     [special-editor-number-section :weapons "Damage" :dmg dmg]
     [special-editor-number-section :weapons "Defense" :def def]
     [special-editor-number-section :weapons "Overwork" :ovw ovw]
     [special-editor-number-section :weapons "Dice Pool" :dice-pool dice-pool]
     [:span.sheet-field
      [:label "Tags"]
      [input-textarea
       :class "weapons-text select-text"
       :style {:flex 0}
       :model (tags-to-str tags)
       :on-change (fn [t] (overwrite-selected-section! :weapons @(:weapons modal-showing-atoms) :tags
                                                       (str-to-tags t)))]]]))

(defn armor-editor []
  (let [{:keys [name soak hardness mp tags] :as the-armor} (nth (get-selected-section :armor) @(:armor modal-showing-atoms))]
    (println the-armor)
    [:div
     [:span.sheet-field
      [:label "Armor Name"]
      [input-text
       :class "armor-text select-text"
       :style {:flex 0}
       :model name
       :on-change #(overwrite-selected-section! :armor (keypath @(:armor modal-showing-atoms) :name name))]
      ]
     [special-editor-number-section :armor "Soak" :soak soak]
     [special-editor-number-section :armor "Hardness" :hardness hardness]
     [special-editor-number-section :armor "MP" :mp mp]
     [:span.sheet-field
      [:label "Tags"]
      [input-textarea
       :class "armor-text select-text"
       :style {:flex 0}
       :model (tags-to-str tags)
       :on-change (fn [t] (overwrite-selected-section! :armor (keypath @(:armor modal-showing-atoms)) :tags
                                                       (str-to-tags t)))]]]))

(defn data-display []
  [input-textarea
   :height (str (- (:height @window-size) 100) "px")
   :width (str (- (:width @window-size) 100) "px")
   :model (pr-str @app-state-new)
   :on-change (fn [n] (reset! app-state-new (edn/read-string n)))])

(defn drop-these [into-this from-this numbahs]
  (let [numbah-set (set numbahs)]
    (->> (range (count from-this))
         (map (fn [n] (when (not (numbah-set n))
                        (nth from-this n))))
         (filter (fn [n] (not (nil? n))))
         (into into-this)
         )))

(defn slot-up [kind-of new-thing]
  (fn [] (overwrite-selected-section! kind-of (fn [x] (into x [new-thing])))))
(defn slot-down [kind-of atomo]
  (fn [] (overwrite-selected-section! kind-of #(drop-these [] % @atomo))
    (reset! atomo #{})))

(defn table-row-for-data
  [column-keys-in-order field-transform-fns individual-map]
  (into [:tr]
        (map (fn [a]
               [:td (if (keyword? a)
                      ((a field-transform-fns) (a individual-map))
                      (((first a) field-transform-fns) ((first a) individual-map)))]
               ) column-keys-in-order)))

(defn column-headers [column-keys-in-order]
  (into [:tr]
        (map (fn [a]
               [:th (if (keyword? a)
                      (-> a (name) (str/replace "-" " ") (str/capitalize))
                      (second a))]
               ) column-keys-in-order)))
(defn table-for-data [column-keys-in-order field-transform-fns seq-of-maps]
  [:table (into
            [:tbody (column-headers column-keys-in-order)]
            (map (fn [a] (table-row-for-data column-keys-in-order field-transform-fns a)) seq-of-maps))])

(defn close-modal
  [keyman hide-value]
  (fn []
    (set-showing-to! keyman hide-value)
    (clear-selected! keyman)))

