(ns anathema.diceroller
  (:require [clojure.string :as str]
            [cljs.pprint :as pp :refer [pprint cl-format]]
            [cljs.tools.reader.edn :as edn]
            [garden.core :as garden]
            [anathema.character-elements :as ch-el]
            [anathema.util :as au]
            [anathema.state :as ast :refer [app-state-new window-size get-selected-thing get-selected-section overwrite-selected-section! modal-showing-atoms set-showing-to! selection-atoms set-selected-to! clear-selected! chron-editor-showing-section chron-editor-editing-now chron-editor-tabs set-chron-editor-to]]
            [com.rpl.specter :as sp :refer [ALL ATOM FIRST LAST select transform keypath srange if-path pred setval]]
            [re-com.core :as re :refer [h-box v-box box gap line radio-button p h-split scroller selection-list input-text input-textarea single-dropdown checkbox radio-button button popover-anchor-wrapper popover-content-wrapper modal-panel horizontal-tabs]]
            [reagent.core :as reagent]))

(declare diceview)

(def ten-range (range 1 11))

(defn roll-d-10 [] (+ 1 (int (rand 10))))

(def min-for-success 7)
(def set-for-success #{7 8 9 10})

(defn get-x-d-10 [x]
  ;(into [] (map (fn [numbah] (roll-d-10)) (range 0 x)))
  (->> roll-d-10 (repeatedly) (take x) (into []))
  )

(def blank-roll-results-map
  {:roll-vecs []
   :results   0})

(defn count-success
  [roll-vec single-set double-set]
  (->> roll-vec
       (map (fn [a] (cond (double-set a) 2
                          (single-set a) 1
                          :else 0
                          )))
       (reduce +)))

(defn count-rerolls [roll-vec reroll-set]
  (let [re-roll-number (count-success roll-vec reroll-set #{})]
    re-roll-number))

(defn count-successes-for-multi-rolls [roll-vecs double-numbers]
  (reduce + (map (fn [a] (count-success a set-for-success double-numbers)) roll-vecs)))

(defn reroll-it [roll-vec reroll-set]
  (let [reroll-number (count-rerolls roll-vec reroll-set)]
    (get-x-d-10 reroll-number)))

(defn make-roll [number-of-dice reroll-numbers]
  (loop [ongoing-vec [(get-x-d-10 number-of-dice)]]
    (if (empty? (last ongoing-vec))
      (into [] (drop-last ongoing-vec))
      (recur (into ongoing-vec [(-> ongoing-vec (last) (reroll-it reroll-numbers))])))
    ))

(defn roll-dice [number-of-dice reroll-set double-set]
  (let [the-rolls (make-roll number-of-dice reroll-set)]
    {:rolls      the-rolls
     :reroll-set reroll-set
     :double-set double-set
     :d10 number-of-dice
     :successes  (count-successes-for-multi-rolls the-rolls double-set)}))

(defn return-at-n [numbahstring n]
  (reduce str (map (fn [a] (if (= (dec n) (mod a n))
                  (str (nth numbahstring a) "\n          ")
                  (nth numbahstring a))) (range (count numbahstring)))))
(defn str-pad-single-number [numbah]
  (if (> 10 numbah)
    (str " " numbah)
    (str numbah)))
(defn make-interspace-reduce-str [interspace-str]
  (fn [a b] (str a interspace-str b)))
(defn str-format-result [{:keys [rolls successes double-set reroll-set d10] :as the-result}]
  (let [spacy (make-interspace-reduce-str " ")
        reduced-rolls (reduce (fn [stri a] (str stri "ROLL " (str-pad-single-number (inc a)) ":  "
                                                (return-at-n (->> a (nth rolls) (sort) (reverse) (map str-pad-single-number) (reduce spacy)) 42) "\n")) "" (range (count rolls)))]
    (str "___________________________________________________\n"

         "--|X|_|O|---------- RESULT:  " (str-pad-single-number successes) " -------------------"
         "\n"
         "‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾\n"
         "#  DICE:  " (str-pad-single-number d10) "\n"
         "ReROLLS:  " (return-at-n (reduce spacy (map str-pad-single-number reroll-set)) 42) "\n"
         "DOUBLES:  " (return-at-n (reduce spacy (map str-pad-single-number double-set)) 42) "\n"
         "---------------------------------------------------\n"
         reduced-rolls
         "___________________________________________________"
         "\n\n")))
(defn make-dice-roller-readout [dice-roller-vecs]
  (reduce #(str %2 %1) (map str-format-result dice-roller-vecs)))
