(ns anathema.util)

(defn name-map [listo]
  (into {} (map (fn [n] {(:name n) n}) listo)))