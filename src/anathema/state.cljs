(ns anathema.state
  (:require-macros
    [cljs.core.async.macros :as asyncmac :refer [go go-loop]])
  (:require
    [cemerick.url :refer [url url-encode url-decode]]
    [cljs.core.async :as async :refer [chan <! >! put!]]
    [cljs.tools.reader.edn :as edn]
    [reagent.core :as reagent]
    [anathema.character-elements :as cha-elm]
    [com.rpl.specter :as sp :refer [ALL ATOM FIRST LAST STAY select transform keypath srange if-path pred]]
    [clojure.string :as str]))

(defn make-new-key []
  (keyword (gensym "anatha-el-")))

(defn mapmap [f s] (into {} (map f s)))
(defn printpass [a] (println a) a)
(defn state-list-to-db-map [state-list]
  (let [nascient-db-map (mapmap (fn [a]
                                  [(make-new-key) a])
                                (subvec state-list 1))
        names-to-keys (mapmap (fn [[k {:keys [name] :as v}]]
                                [name k])
                              nascient-db-map)
        db-map-name-replaced (mapmap (fn [[k {:keys [name type chron] :as v}]]
                                       [k
                                        (if chron
                                          (assoc v :chron (get names-to-keys chron))
                                          v)])
                                     nascient-db-map)]
    (assoc db-map-name-replaced :selected (get names-to-keys (first state-list)))))
(defn db-map-to-state-list [db-map]
  (into [(:name ((:selected db-map) db-map))] (map (fn [[k v]]
                                                     (let [{:keys [name type chron]} v
                                                           changed-v (if chron
                                                                       (assoc v :chron (:name (chron db-map)))
                                                                       v)]
                                                       changed-v))
                                                   (dissoc db-map :selected))))

(def dev-state-list
  [;"Volfer"
   "Volfer"
   cha-elm/volfer
   {:name "Rajmael", :type :character :character-type :solar :caste :twilight}
   {:name "Liger", :type :character :character-type :demon :caste :first-circle}
   {:name "Alkiah" :type :character :character-type :solar :caste :dawn}
   cha-elm/tomb-of-dreams
   {:name "Under Heaven's Eye", :type :chron :character-type :mixed}
   {:name "The Giver's Choice", :type :chron :character-type :solar}
   {:name "A Huge Amount of Nothing", :type :chron  :character-type :abyssal}
   {:name "Characters" :type :category :category :character}
   {:name "Chronicles" :type :category :category :chron}
   {:name "Utilities" :type :category :category :utility}]
  )
(defonce app-state-new (reagent/atom
                         (state-list-to-db-map dev-state-list)))

(swap! app-state-new (fn [a] (assoc a :diceroller {:name  "Dice Roller"
                                                   :type  :utility
                                                   :id    :diceroller
                                                   :rolls []
                                                   })))

(def layout-state-atom (reagent/atom {:search true}))

(defn toggle-showing-modal [modal-key]
  (swap! layout-state-atom #(assoc % modal-key (not (modal-key %)))))
(defn get-layout-state
  ([] @layout-state-atom)
  ([thingy] (thingy @layout-state-atom)))



(defn make-chron-key [reg-key]
  (keyword (str "chron-" (name reg-key))))

(defn get-state-as-vec []
  (map (fn [[a b]] b) @app-state-new))
(defn switch-editing-focus-to [name-of-thing]
  (let [edit-this-key (->> @app-state-new (filter #(= (-> % (second) (:name)) name-of-thing)) (first) (first))]
    (swap! app-state-new (fn [a] (assoc a :selected edit-this-key)))))
(defn get-selected-thing []
  ((:selected @app-state-new) @app-state-new))
(defn get-selected-section [selected-key]
  (let [selected-thing (get-selected-thing)]
    (selected-key selected-thing)))
(defn overwrite-selected-section!
  ([section-key new-thing] (overwrite-selected-section! section-key STAY new-thing))
  ([section-key sub-key new-thing] (overwrite-selected-section! section-key sub-key STAY new-thing))
  ([section-key sub-key penta-key new-thing] (overwrite-selected-section! section-key sub-key penta-key STAY new-thing))
  ([section-key sub-key penta-key supah-sub-key new-thing]
   (transform [ATOM (keypath (:selected @app-state-new)) section-key sub-key penta-key supah-sub-key]
              (fn [a] (if (fn? new-thing) (new-thing a)
                                          new-thing))
              app-state-new))
  )

(defonce window-size (reagent/atom {:width 100 :height 100 :split-place 16}))
(defn get-left-section []
  (* (* .01 (:width @window-size)) (:split-place @window-size)))
(defn get-right-section []
  (- (:width @window-size) (get-left-section)))
(defn on-window-resize [evt]
  (swap! window-size (fn [orig]
                       (into orig {:width  (.-innerWidth js/window)
                                   :height (.-innerHeight js/window)}))))
(on-window-resize nil)

(defn- get-base-url [] (url (str (-> js/window .-location))))
(defn get-current-state-url []
  (let [base-url (get-base-url)
        ]))

(def modal-showing-atoms
  (into {} (map (fn [[a b]] [a (reagent/atom b)])
                [[:data false]
                 [:edit-other false]
                 [:weapons -1]
                 [:armor -1]
                 [:bulk-charms false]
                 [(make-chron-key :charms) -1]])))
(defn set-showing-to! [keyman newval]
  (reset! (keyman modal-showing-atoms) newval))

(def chron-editor-showing-section (reagent/atom :charms))
(def chron-editor-tabs [:charms
                        :merits
                        :weapons
                        :armor
                        ])
(def chron-editor-editing-now
  (reagent/atom (into {} (map (fn [a] {a 1}) chron-editor-tabs))))
(defn set-chron-editor-to
  ([section-key] (reset! chron-editor-showing-section (hash section-key)))
  ([section-key edit-index]
   (set-chron-editor-to section-key)
   (swap! chron-editor-editing-now (fn [a] (assoc a section-key edit-index)))))

(def selection-atoms
  (into {} (map (fn [a] [a (reagent/atom #{})])
                [:intimacies
                 :charms
                 (make-chron-key :charms)
                 :merits
                 :weapons
                 :armor
                 :evocations])))
(defn set-selected-to! [keyman newval]
  (reset! (keyman modal-showing-atoms) newval))
(defn add-to-selected [keyman newval]
  (swap! (keyman selection-atoms)
         #(into % [newval])))
(defn remove-from-selected [keyman oldval]
  (swap! (keyman selection-atoms)
         (fn [a] (into #{} (remove #(= % oldval) a)))))
(defn clear-selected! [keyman]
  (if ((into #{} (keys selection-atoms)) keyman)
    (set-selected-to! keyman #{})))