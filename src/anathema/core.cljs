(ns anathema.core
    (:require [reagent.core :as reagent :refer [atom]]
              [anathema.components.app-face :as acaf :refer [app-face]]
              [anathema.character-elements :as cha-elm]
              [anathema.styles :as style]
              [anathema.state :as state :refer [on-window-resize]]))

(enable-console-print!)

(println "This text is printed from src/anathema/core.cljs. Go ahead and edit it and see reloading in action.")

;; define your app data so that it doesn't get over-written on reload




(reagent/render-component [app-face]
                          (. js/document (getElementById "app")))
(.addEventListener js/window "resize" on-window-resize)
(defn on-js-reload []

  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
)
