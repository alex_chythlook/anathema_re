(ns anathema.data-imp
  (:require [anathema.character-elements :as cha-elm]))

(defn make-new-key []
  (keyword (gensym "anatha-el-")))
(defn mapmap [f s] (into {} (map f s)))
(defn state-list-to-db-map [state-list]
  (let [nascient-db-map (mapmap (fn [a]
                                  [(make-new-key) a])
                                state-list)
        names-to-keys (mapmap (fn [[k {:keys [name] :as v}]]
                                [name k])
                              nascient-db-map)
        db-map-name-replaced (mapmap (fn [[k {:keys [name type chron] :as v}]]
                                       [k
                                        (if chron
                                          (assoc v :chron (get names-to-keys chron))
                                          v)])
                                     nascient-db-map)]))

(def ^{:private true} dev-state-list
  [;"Volfer"
   cha-elm/volfer
   {:name "Rajmael", :type :character :character-type :solar :caste :twilight}
   {:name "Liger", :type :character :character-type :demon :caste :first-circle}
   {:name "Alkiah" :type :character :character-type :solar :caste :dawn}
   cha-elm/tomb-of-dreams
   {:name "Under Heaven's Eye", :type :chron :character-type :mixed}
   {:name "The Giver's Choice", :type :chron :character-type :solar}
   {:name "A Huge Amount of Nothing", :type :chron :character-type :abyssal}
   {:name "Characters" :type :category :category :character}
   {:name "Chronicles" :type :category :category :chron}
   {:name "Utilities" :type :category :category :utility}]
  )

(defn find-indices-for [pred coll]
  (reduce (fn [a b] (if (pred (nth coll b)) (conj a b) a)) [] (range (count coll))))

(defn find-index-for [item coll]
  (first (find-indices-for #(= % item) coll)))